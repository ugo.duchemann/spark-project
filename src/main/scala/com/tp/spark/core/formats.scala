package com.tp.spark.core

import java.net.URI
import java.time.Instant
import java.time.ZonedDateTime
import spray.json._


object Formats extends DefaultJsonProtocol {
  implicit val userIdFormat = DefaultJsonProtocol.jsonFormat1(Id[User])
  implicit val postIdFormat = DefaultJsonProtocol.jsonFormat1(Id[Post])
  implicit val messageIdFormat = DefaultJsonProtocol.jsonFormat1(Id[Message])

  implicit object URIFormat extends JsonFormat[URI] {
    def write(o: URI) = JsString(o.toString)

    def read(value: JsValue) = value match {
      case JsString(value) =>
        new URI(value)
      case other => deserializationError("URI expected got: " + other)
    }
  }

  implicit object InstantFormat extends JsonFormat[Instant] {
    def write(o: Instant) = JsString(o.toString)

    def read(value: JsValue) = value match {
      case JsString(value) =>
        ZonedDateTime.parse(value).toInstant()
      case other => deserializationError("Instant expected got: " + other)
    }
  }

  implicit object UserProtocol extends JsonFormat[User] {
    def write(o: User) = JsObject(
      "id" -> o.id.toJson,
      "name" -> JsString(o.name),
      "image" -> o.image.toJson,
      "deleted" -> JsBoolean(o.deleted),
      "verified" -> JsBoolean(o.verified),
      "updatedOn" -> o.updatedOn.toJson
    )

  def read(value: JsValue) = {
    value.asJsObject.getFields("id", "name", "image", "deleted", "verified", "updatedOn") match {
      case Seq(id, JsString(name), image, JsBoolean(deleted), JsBoolean(verified), updatedOn) =>
        new User(id.convertTo[Id[User]], name, image.convertTo[URI], deleted, verified, updatedOn.convertTo[Instant])
      case other => deserializationError("User expected got: " + other)
    }
   }
  }

  implicit object PostProtocol extends JsonFormat[Post] {
    def write(o: Post) = JsObject(
      "id" -> o.id.toJson,
      "text" -> JsString(o.text),
      "author" -> o.author.toJson,
      "deleted" -> JsBoolean(o.deleted),
      "updatedOn" -> o.updatedOn.toJson
    )

  def read(value: JsValue) = {
    value.asJsObject.getFields("id", "text", "author", "deleted", "updatedOn") match {
      case Seq(id, JsString(text), author, JsBoolean(deleted), updatedOn) =>
        new Post(id.convertTo[Id[Post]], text, author.convertTo[Id[User]], deleted, updatedOn.convertTo[Instant])
      case other => deserializationError("Post expected got: " + other)
    }
   }
  }

  implicit object MessageProtocol extends JsonFormat[Message] {
    def write(o: Message) = JsObject(
      "id" -> o.id.toJson,
      "text" -> JsString(o.text),
      "sender" -> o.sender.toJson,
      "receiver" -> o.receiver.toJson,
      "deleted" -> JsBoolean(o.deleted),
      "updatedOn" -> o.updatedOn.toJson
    )

  def read(value: JsValue) = {
    value.asJsObject.getFields("id", "text", "sender", "receiver", "deleted", "updatedOn") match {
      case Seq(id, JsString(text), sender, receiver, JsBoolean(deleted), updatedOn) =>
        new Message(id.convertTo[Id[Message]], text, sender.convertTo[Id[User]], receiver.convertTo[Id[User]], deleted, updatedOn.convertTo[Instant])
      case other => deserializationError("Message expected got: " + other)
    }
   }
  }
}
