package com.tp.spark.core

import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.kstream.KStream


/*class Consumer[V](topic: String) {
  val props = new Properties()
  props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
  props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")

  val consumer = new KafkaConsumer[Id[V], V](props)
  consumer.subscribe(Collections.singletonList(topic))
}

object Consumers {
  val userConsumer = new Consumer[User]("users")
  val postConsumer = new Consumer[Post]("consumers")
  val messageConsumer = new Consumer[Message]("messages")
}*/

class Stream[K, V] {
  val streamsBuilder = new StreamsBuilder()
  def stream(value: String)(implicit record: Record[K, V]): KStream[K, V] = {
    streamsBuilder.stream[K, V](value)
  }
}

object Streams extends App {
  println("Starting Users Stream")
  val usersStream = new Stream[String, User]().stream("users")
  println("Starting Posts Stream")
  val postsStream = new Stream[String, Post]().stream("posts")
  println("Starting Messages Stream")
  val messagesStream = new Stream[String, Message]().stream("messages")
}