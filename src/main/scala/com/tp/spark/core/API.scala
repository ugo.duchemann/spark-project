package com.tp.spark.core

import java.net.URI
import java.time.Instant

object API extends App{
  println("Choose an option:")
  println("1. Create User")
  println("2. Create Post")
  println("3. Create Message")
  println("4. Create User Stream")
  println("5. Create Post Stream")
  println("6. Create Message Stream")

  val option = scala.io.StdIn.readLine().toInt

  if (option == 1){
    print("Enter User Id: ")
    val id = scala.io.StdIn.readLine().toString
    print("Enter User Name: ")
    val name = scala.io.StdIn.readLine().toString
    print("Enter User image URI: ")
    val uri = scala.io.StdIn.readLine().toString
    new User(Id[User](id), name, URI.create(uri), false, false, Instant.now())
    println("User " + id + " Created successfully !")
  }
  else if (option == 2){
    print("Enter Post Id: ")
    val id = scala.io.StdIn.readLine().toString
    print("Enter Post text: ")
    val text = scala.io.StdIn.readLine().toString
    print("Enter Author Id: ")
    val aid = scala.io.StdIn.readLine().toString
    new Post(Id[Post](id), text, Id[User](aid), false, Instant.now())
    println("Post " + id + " Created successfully !")
  }
  else if (option == 3){
    print("Enter Message Id: ")
    val id = scala.io.StdIn.readLine().toString
    print("Enter Message text: ")
    val text = scala.io.StdIn.readLine().toString
    print("Enter Sender Id: ")
    val sender = scala.io.StdIn.readLine().toString
    print("Enter Receiver Id: ")
    val receiver = scala.io.StdIn.readLine().toString
    new Message(Id[Message](id), text, Id[User](sender), Id[User](receiver), false, Instant.now())
    println("Message " + id + " Created successfully !")
  }
  else if (option == 4){
    print("Enter User Stream Topic: ")
    val topic = scala.io.StdIn.readLine().toString
    new Stream[String, User]().stream(topic)
    println("New User Stream Started Successfully on topic " + topic)
  }
  else if (option == 5){
    print("Enter Post Stream Topic: ")
    val topic = scala.io.StdIn.readLine().toString
    new Stream[String, Post]().stream(topic)
    println("New Post Stream Started Successfully on topic " + topic)
  }
  else if (option == 6){
    print("Enter Message Stream Topic: ")
    val topic = scala.io.StdIn.readLine().toString
    new Stream[String, Message]().stream(topic)
    println("New Message Stream Started Successfully on topic " + topic)
  }
  else {
    println("Wrong option :(")
  }

  /*println("Starting User Producer")
  val userProducer = new Producer[User]()
  val user = new User(
    Id[User]("user0"),
    "some name",
    URI.create("https://some-uri"),
    false,
    false,
    Instant.now()
  )

  userProducer.send(user)
  userProducer.close()
  println("User Producer Closed")

  println("Starting Post Producer")
  val postProducer = new Producer[Post]()
  val post = new Post(
    Id[Post]("post0"),
    "some text",
    Id[User]("user0"),
    false,
    Instant.now()
  )
  postProducer.send(post)
  postProducer.close()
  println("Post Producer Closed")*/
}