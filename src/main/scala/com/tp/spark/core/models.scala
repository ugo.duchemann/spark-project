package com.tp.spark.core

import java.net.URI
import java.time.Instant

trait Record[K, V] {
  def topic: String
  def key(value: V): String
  def timestamp(value: V): Long
}

case class Id[Resource](value: String) extends AnyVal
case class User(id: Id[User], name: String, image: URI, deleted: Boolean, verified: Boolean, updatedOn: Instant)
case class Post(id: Id[Post], text: String, author: Id[User], deleted: Boolean, updatedOn: Instant)
case class Message(id: Id[Message], text: String, sender: Id[User], receiver: Id[User], deleted: Boolean, updatedOn: Instant)


object User {
  implicit val record: Record[String, User] = new Record[String, User] {
    val topic = "users"
    def key(user: User): String = user.id.value
    def timestamp(user: User): Long = user.updatedOn.toEpochMilli
  }
}

object Post {
  implicit val record: Record[String, Post] = new Record[String, Post] {
    val topic = "posts"
    def key(post: Post): String = post.id.value
    def timestamp(post: Post): Long = post.updatedOn.toEpochMilli
  }
}

object Message {
  implicit val record: Record[String, Message] = new Record[String, Message] {
    val topic = "messages"
    def key(message: Message): String = message.id.value
    def timestamp(message: Message): Long = message.updatedOn.toEpochMilli
  }
}


