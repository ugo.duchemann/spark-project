package com.tp.spark.core

import java.io.{BufferedWriter, FileWriter}

import org.apache.spark.{SparkConf, SparkContext}
import spray.json._


class wUserToFile(user: User) {
  import Formats._
  val w = new BufferedWriter(new FileWriter("outputuser.txt"))
  w.write(user.toJson.prettyPrint)
}

class wPostToFile(post: Post) {
  import Formats._
  val w = new BufferedWriter(new FileWriter("outputpost.txt"))
  w.write(post.toJson.prettyPrint)
}

class wMessageToFile(message: Message) {
  import Formats._
  val w = new BufferedWriter(new FileWriter("outputmessage.txt"))
  w.write(message.toJson.prettyPrint)
}

class fromJson(path: String) {
  val conf = new SparkConf()
    .setAppName("FromJson")
    .setMaster("local[*]")
  val sc = SparkContext.getOrCreate(conf)
  val finalrdd = sc.textFile(path)
}
