package com.tp.spark.core

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}


class Producer[V] {
  val config = new Properties()
  config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.Serializer")

  val kafkaProducer = new KafkaProducer[String, V](config)

  def send(value: V)(implicit record: Record[String, V]) = {
    val r = new ProducerRecord(record.topic, null, record.timestamp(value), record.key(value), value)
    kafkaProducer.send(r).get()
  }

  def close() = {
    kafkaProducer.close()
  }
}