package com.tp.spark.core

import java.util

import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}
import spray.json._

class JsonSerializer[T >: Null <: Any : JsonFormat] extends Serializer[T] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()

  override def serialize(topic: String, data: T): Array[Byte] = {
    data.toJson.compactPrint.getBytes()
  }

  override def close(): Unit = ()
}

class JsonDeserializer[T >: Null <: Any : JsonFormat] extends Deserializer[T] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()

  override def deserialize(topic: String, data: Array[Byte]): T = {
    data.map(_.toChar).mkString.parseJson.convertTo[T]
  }

  override def close(): Unit = ()
}

class JsonSerde[T >: Null <: Any : JsonFormat] extends Serde[T] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()

  override def close(): Unit = ()

  override def serializer(): Serializer[T] = new JsonSerializer[T]

  override def deserializer(): Deserializer[T] = new JsonDeserializer[T]
}
