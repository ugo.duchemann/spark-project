name := "Spark Project"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.0.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.0.0"
libraryDependencies += "org.apache.kafka" % "kafka-streams" % "1.1.0"
libraryDependencies += "io.spray" %% "spray-json" % "1.3.3"
